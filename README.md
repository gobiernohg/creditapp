# APP

# Instalación

Utiliza el gestor de paquetes  [yarn](https://yarnpkg.com/) para instalar las dependencias.

```bash
yarn install
```

# Inicia app

```bash
react-native run-ios
react-native run-android
```