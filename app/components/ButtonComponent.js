import React, {Component} from 'react';
import {View, Text, TouchableHighlight} from 'react-native';

type ButtonComponentProps={
  action:()=>void,
  title:string,
  width:Number,
  color:string,
  colorText:string
}

type ButtonComponentState={

}

class ButtonComponent extends Component<ButtonComponentProps,ButtonComponentState>{
  render(){
    return(
      <TouchableHighlight
        underlayColor={'transparent'}
        onPress={this.props.action}
        style={{backgroundColor:this.props.color, borderRadius:10, marginVertical:10,
        height:52, width:this.props.width, alignItems:'center', justifyContent:'center', marginHorizontal:10}}>
        <Text style={{fontSize:14, color:this.props.colorText}}>{this.props.title}</Text>
      </TouchableHighlight>
    )
  }
}
export default ButtonComponent