const color = {
  grayDark:'#232530',
  grayLine:'#455B6314',
  shadow: 'rgba(0,0,0,0.85)',
  gray: 'rgba(255,255,255,0.4)',
  grayIcon: '#D6D6D6',
  white:'#ffffff',
  grayTransparent: 'rgba(255,255,255,0.1)',
  green:'#DCF5DA',
  greenSecond:'#E1F5EC',
  blueOne:'#DEF0FA',
  blueFinal:'#DEEAFB',

  colorSeparatorOne:'#51E85E',
  colorSeparatorTwo:'#88E16A',
  colorSeparatorThree:'#6FDCF8',
  colorSeparatorFour:'#6E97FD',

  colorBlueOne:'#69B9E3',
  colorBlueTwo:'#5FABE1',
  colorBlueThree: '#559EDF',
  red: '#E2493C',
  yellow:'#F9BA09',
  
};

export default color;
