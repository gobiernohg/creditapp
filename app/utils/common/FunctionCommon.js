import {Alert} from 'react-native'
export const messageAlert = (message) => {
  return(
    Alert.alert(
    'Credit',
    message,
    [
      {
        text: "OK", onPress: () => {}
      },
    ],
    {cancelable: false}
  )
  )
};

export const AlertErrorCallBack = (message,action) => {
  return(
    Alert.alert(
    'Credit',
    message,
    [
      {
        text: 'OK', 
        onPress: action
      },
    ],
    {cancelable: false}
  )
  )
};

export const numberToMoney =(num)=>{
  let numero = Number(num)
  let money = (numero).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
  return money = '$'+money
}