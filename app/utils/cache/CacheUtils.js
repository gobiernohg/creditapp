import AsyncStorage from '@react-native-community/async-storage';

const SEE_LANDING = 'SEE_LANDING';


type CacheType = {

  setSeeLanding: (any) => void,
  getSeeLanding: ()=>Promise<string>,

  removeAll:(any)=>void,
};

const CacheUtil: CacheType = {
 
  setSeeLanding: (seeLanding:string ) => {
    return AsyncStorage.setItem( SEE_LANDING, seeLanding.toString() );
  },

  getSeeLanding: async (): Promise<string> => {
    return await AsyncStorage.getItem(SEE_LANDING);
  },


  removeAll: (callback) => {
    let keys = [SEE_LANDING];
    return AsyncStorage.multiRemove(keys, callback);
  },
  
};

export default CacheUtil;
