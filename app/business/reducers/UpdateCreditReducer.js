import {
  UPDATE_CREDIT_SUCCESS,
  UPDATE_CREDIT_ERROR,
  UPDATE_CREDIT_PURGE,
} from '../types';

const stateUpdateCredit = {
  updateCreditData: {},
  updateCreditError: {},
};

export default UpdateCredit = (state = stateUpdateCredit, action) => {
  switch (action.type) {
    case UPDATE_CREDIT_SUCCESS:
      return {...state, updateCreditData: action.payload};
    case UPDATE_CREDIT_ERROR:
      return {...state, updateCreditError: action.payload};
    case UPDATE_CREDIT_PURGE:
      return {...stateUpdateCredit, purgeUpdateCredit: action.payload};
  }
  return state;
}