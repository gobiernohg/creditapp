import {
  GET_USER_SUCCESS,
  GET_USER_ERROR,
  GET_USER_PURGE,
} from '../types';

const stateGetUser = {
  getUserData: {},
  getUserError: {},
};

export default GetUser = (state = stateGetUser, action) => {
  switch (action.type) {
    case GET_USER_SUCCESS:
      return {...state, getUserData: action.payload};
    case GET_USER_ERROR:
      return {...state, getUserError: action.payload};
    case GET_USER_PURGE:
      return {...stateGetUser, purgeGetUser: action.payload};
  }
  return state;
}