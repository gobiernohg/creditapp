import {
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  LOGIN_PURGE,
} from '../types';

const stateLogin = {
  loginData: {},
  loginError: {},
};

export default Login = (state = stateLogin, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {...state, loginData: action.payload};
    case LOGIN_ERROR:
      return {...state, loginError: action.payload};
    case LOGIN_PURGE:
      return {...stateLogin, purgeLogin: action.payload};
  }
  return state;
}