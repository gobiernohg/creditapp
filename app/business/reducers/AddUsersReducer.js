import {
  ADD_USERS_SUCCESS,
  ADD_USERS_ERROR,
  ADD_USERS_PURGE,
} from '../types';

const stateAddUsers = {
  addUsersData: {},
  addUsersError: {},
};

export default AddUsers = (state = stateAddUsers, action) => {
  switch (action.type) {
    case ADD_USERS_SUCCESS:
      return {...state, addUsersData: action.payload};
    case ADD_USERS_ERROR:
      return {...state, addUsersError: action.payload};
    case ADD_USERS_PURGE:
      return {...stateAddUsers, purgeAddUsers: action.payload};
  }
  return state;
}