import {
  ADD_CREDIT_SUCCESS,
  ADD_CREDIT_ERROR,
  ADD_CREDIT_PURGE,
} from '../types';

const stateAddCredit = {
  addCreditData: {},
  addCreditError: {},
};

export default AddCredit = (state = stateAddCredit, action) => {
  switch (action.type) {
    case ADD_CREDIT_SUCCESS:
      return {...state, addCreditData: action.payload};
    case ADD_CREDIT_ERROR:
      return {...state, addCreditError: action.payload};
    case ADD_CREDIT_PURGE:
      return {...stateAddCredit, purgeAddCredit: action.payload};
  }
  return state;
}