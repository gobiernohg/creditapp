import {
  GET_ALL_USERS_SUCCESS,
  GET_ALL_USERS_ERROR,
  GET_ALL_USERS_PURGE,
} from '../types';

const stateGetAllUsers = {
  getAllUsersData: {},
  getAllUsersError: {},
};

export default GetAllUsers = (state = stateGetAllUsers, action) => {
  switch (action.type) {
    case GET_ALL_USERS_SUCCESS:
      return {...state, getAllUsersData: action.payload};
    case GET_ALL_USERS_ERROR:
      return {...state, getAllUsersError: action.payload};
    case GET_ALL_USERS_PURGE:
      return {...stateGetAllUsers, purgeGetAllUsers: action.payload};
  }
  return state;
}