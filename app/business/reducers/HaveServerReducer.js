import {
  HAVE_SERVER_SUCCESS,
  HAVE_SERVER_ERROR,
  HAVE_SERVER_PURGE,
} from '../types';

const stateHaveServer = {
  haveServerData: {},
  haveServerError: {},
};

export default HaveServer = (state = stateHaveServer, action) => {
  switch (action.type) {
    case HAVE_SERVER_SUCCESS:
      return {...state, haveServerData: action.payload};
    case HAVE_SERVER_ERROR:
      return {...state, haveServerError: action.payload};
    case HAVE_SERVER_PURGE:
      return {...stateHaveServer, purgeHaveServer: action.payload};
  }
  return state;
}