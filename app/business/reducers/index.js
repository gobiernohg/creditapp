import {combineReducers} from 'redux';
import HaveServerReducer from './HaveServerReducer';
import LoginReducer from "./LoginReducer";
import GetAllUsersReducer from "./GetAllUsersReducer";
import AddUsersReducer from './AddUsersReducer';
import DeleteUserReducer from './DeleteUserReducer';
import AddCreditReducer from './AddCreditReducer';
import UpdateCreditReducer from './UpdateCreditReducer';
import GetUserReducer from './GetUserReducer';

const rootReducer = combineReducers({
  haveServer: HaveServerReducer,
  login:LoginReducer,
  getAllUsers:GetAllUsersReducer,
  addUsers:AddUsersReducer,
  deleteUser:DeleteUserReducer,
  addCredit:AddCreditReducer,
  updateCredit:UpdateCreditReducer,
  getUser:GetUserReducer
});

export default rootReducer;