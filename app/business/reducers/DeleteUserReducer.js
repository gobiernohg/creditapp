import {
  DELETE_USER_SUCCESS,
  DELETE_USER_ERROR,
  DELETE_USER_PURGE,
} from '../types';

const stateDeleteUser = {
  deleteUserData: {},
  deleteUserError: {},
};

export default DeleteUser = (state = stateDeleteUser, action) => {
  switch (action.type) {
    case DELETE_USER_SUCCESS:
      return {...state, deleteUserData: action.payload};
    case DELETE_USER_ERROR:
      return {...state, deleteUserError: action.payload};
    case DELETE_USER_PURGE:
      return {...stateDeleteUser, purgeDeleteUser: action.payload};
  }
  return state;
}