
import { ADD_USERS } from '../endPoints'
import {
  ADD_USERS_SUCCESS,
  ADD_USERS_ERROR,
  ADD_USERS_PURGE
} from '../types'


export const AddUsersAction = (data, token) => {
  
  const config={
    method: 'POST',
    body:JSON.stringify(data),
    headers:{
      'access-token': token
    }
  };
  
  console.log('RECUEST:',data, config, ADD_USERS);
  
  return ( dispatch ) => {

    if(data === 'PURGE'){
      dispatch( { type: ADD_USERS_PURGE, payload: 'PURGE'} )
    }else{
      fetch(ADD_USERS ,config)
        .then(json => json.json())
        .then((responseJson) => {
          if(responseJson){
            const response = {
              isOK:true,
              AddUsers: responseJson
            }
            console.log('RESPONSE ADD_USERS DATA: ',response);
            dispatch( { type: ADD_USERS_SUCCESS, payload: response} )
          }else{
            const response = {
              isOK: false,
              error: responseJson
            }
            console.log('ERROR ADD_USERS DATA: ',response);
            dispatch( { type: ADD_USERS_ERROR, payload: response} )
          }
          
        })
        .catch((error) => {
          const responseError = {
            isOK: false,
            error: error
          }
          console.log('ERROR ADD_USERS DATA: ',responseError);
          dispatch({ type: ADD_USERS_ERROR, payload: responseError })
        });
    }

  }

};