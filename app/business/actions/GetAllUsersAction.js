
import { GET_ALL_USERS } from '../endPoints'
import {
  GET_ALL_USERS_SUCCESS,
  GET_ALL_USERS_ERROR,
  GET_ALL_USERS_PURGE
} from '../types'


export const getAllUsersAction = (data, token) => {
  
  const config={
    method: 'POST',
    body:JSON.stringify(data),
    headers:{
      'access-token': token
    }
  };
  
  console.log('RECUEST:',data, config, GET_ALL_USERS);
  
  return ( dispatch ) => {

    if(data === 'PURGE'){
      dispatch( { type: GET_ALL_USERS_PURGE, payload: 'PURGE'} )
    }else{
      fetch(GET_ALL_USERS ,config)
        .then(json => json.json())
        .then((responseJson) => {
          if(responseJson){
            const response = {
              isOK:true,
              AllUsers: responseJson
            }
            console.log('RESPONSE GET_ALL_USERS DATA: ',response);
            dispatch( { type: GET_ALL_USERS_SUCCESS, payload: response} )
          }else{
            const response = {
              isOK: false,
              error: responseJson
            }
            console.log('ERROR GET_ALL_USERS DATA: ',response);
            dispatch( { type: GET_ALL_USERS_ERROR, payload: response} )
          }
          
        })
        .catch((error) => {
          const responseError = {
            isOK: false,
            error: error
          }
          console.log('ERROR GET_ALL_USERS DATA: ',responseError);
          dispatch({ type: GET_ALL_USERS_ERROR, payload: responseError })
        });
    }

  }

};