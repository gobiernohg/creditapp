
import { LOGIN } from '../endPoints'
import {
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  LOGIN_PURGE
} from '../types'


export const loginAction = (data) => {
  
  const config={
    method: 'POST',
    body:JSON.stringify(data)
  };
  
  console.log('RECUEST:',data, config, LOGIN);
  
  return ( dispatch ) => {

    if(data === 'PURGE'){
      dispatch( { type: LOGIN_PURGE, payload: 'PURGE'} )
    }else{
      fetch(LOGIN ,config)
        .then(json => json.json())
        .then((responseJson) => {
          if(responseJson){
            const response = {
              isOK:true,
              Login: responseJson
            }
            console.log('RESPONSE LOGIN DATA: ',response);
            dispatch( { type: LOGIN_SUCCESS, payload: response} )
          }else{
            const response = {
              isOK: false,
              error: responseJson
            }
            console.log('ERROR LOGIN DATA: ',response);
            dispatch( { type: LOGIN_ERROR, payload: response} )
          }
          
        })
        .catch((error) => {
          const responseError = {
            isOK: false,
            error: error
          }
          console.log('ERROR LOGIN DATA: ',responseError);
          dispatch({ type: LOGIN_ERROR, payload: responseError })
        });
    }

  }

};