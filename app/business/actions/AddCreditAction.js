
import { ADD_CREDIT } from '../endPoints'
import {
  ADD_CREDIT_SUCCESS,
  ADD_CREDIT_ERROR,
  ADD_CREDIT_PURGE
} from '../types'


export const AddCreditAction = (data, token) => {
  
  const config={
    method: 'POST',
    body:JSON.stringify(data),
    headers:{
      'access-token': token
    }
  };
  
  console.log('RECUEST:',data, config, ADD_CREDIT);
  
  return ( dispatch ) => {

    if(data === 'PURGE'){
      dispatch( { type: ADD_CREDIT_PURGE, payload: 'PURGE'} )
    }else{
      fetch(ADD_CREDIT ,config)
        .then(json => json.json())
        .then((responseJson) => {
          if(responseJson){
            const response = {
              isOK:true,
              AddCredit: responseJson
            }
            console.log('RESPONSE ADD_CREDIT DATA: ',response);
            dispatch( { type: ADD_CREDIT_SUCCESS, payload: response} )
          }else{
            const response = {
              isOK: false,
              error: responseJson
            }
            console.log('ERROR ADD_CREDIT DATA: ',response);
            dispatch( { type: ADD_CREDIT_ERROR, payload: response} )
          }
          
        })
        .catch((error) => {
          const responseError = {
            isOK: false,
            error: error
          }
          console.log('ERROR ADD_CREDIT DATA: ',responseError);
          dispatch({ type: ADD_CREDIT_ERROR, payload: responseError })
        });
    }

  }

};