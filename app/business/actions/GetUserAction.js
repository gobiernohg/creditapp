
import { GET_USER } from '../endPoints'
import {
  GET_USER_SUCCESS,
  GET_USER_ERROR,
  GET_USER_PURGE
} from '../types'


export const getUserAction = (data, token) => {
  
  const config={
    method: 'POST',
    body:JSON.stringify(data),
    headers:{
      'access-token': token
    }
  };
  
  console.log('RECUEST:',data, config, GET_USER);
  
  return ( dispatch ) => {

    if(data === 'PURGE'){
      dispatch( { type: GET_USER_PURGE, payload: 'PURGE'} )
    }else{
      fetch(GET_USER ,config)
        .then(json => json.json())
        .then((responseJson) => {
          if(responseJson){
            const response = {
              isOK:true,
              GetUser: responseJson
            }
            console.log('RESPONSE GET_USER DATA: ',response);
            dispatch( { type: GET_USER_SUCCESS, payload: response} )
          }else{
            const response = {
              isOK: false,
              error: responseJson
            }
            console.log('ERROR GET_USER DATA: ',response);
            dispatch( { type: GET_USER_ERROR, payload: response} )
          }
          
        })
        .catch((error) => {
          const responseError = {
            isOK: false,
            error: error
          }
          console.log('ERROR GET_USER DATA: ',responseError);
          dispatch({ type: GET_USER_ERROR, payload: responseError })
        });
    }

  }

};