
import { URL_SERVER } from '../endPoints'
import {
  HAVE_SERVER_SUCCESS,
  HAVE_SERVER_ERROR,
  HAVE_SERVER_PURGE
} from '../types'


export const haveServerAction = (data) => {
  
  const config={
    method: 'GET',
  };
  
  console.log('RECUEST:',data, config, URL_SERVER);
  
  return ( dispatch ) => {

    if(data === 'PURGE'){
      dispatch( { type: HAVE_SERVER_PURGE, payload: 'PURGE'} )
    }else{
      fetch(URL_SERVER ,config)
        .then(json => json.json())
        .then((responseJson) => {
          if(responseJson){
            const response = {
              isOK:true,
              HaveServer: responseJson
            }
            console.log('RESPONSE HAVE_SERVER DATA: ',response);
            dispatch( { type: HAVE_SERVER_SUCCESS, payload: response} )
          }else{
            const response = {
              isOK: false,
              error: responseJson
            }
            console.log('ERROR HAVE_SERVER DATA: ',response);
            dispatch( { type: HAVE_SERVER_ERROR, payload: response} )
          }
          
        })
        .catch((error) => {
          const responseError = {
            isOK: false,
            error: error
          }
          console.log('ERROR HAVE_SERVER DATA: ',responseError);
          dispatch({ type: HAVE_SERVER_ERROR, payload: responseError })
        });
    }

  }

};