
import { UPDATE_CREDIT } from '../endPoints'
import {
  UPDATE_CREDIT_SUCCESS,
  UPDATE_CREDIT_ERROR,
  UPDATE_CREDIT_PURGE
} from '../types'


export const updateCreditAction = (data, token) => {
  
  const config={
    method: 'POST',
    body:JSON.stringify(data),
    headers:{
      'access-token': token
    }
  };
  
  console.log('RECUEST:',data, config, UPDATE_CREDIT);
  
  return ( dispatch ) => {

    if(data === 'PURGE'){
      dispatch( { type: UPDATE_CREDIT_PURGE, payload: 'PURGE'} )
    }else{
      fetch(UPDATE_CREDIT ,config)
        .then(json => json.json())
        .then((responseJson) => {
          if(responseJson){
            const response = {
              isOK:true,
              UpdateCredit: responseJson
            }
            console.log('RESPONSE UPDATE_CREDIT DATA: ',response);
            dispatch( { type: UPDATE_CREDIT_SUCCESS, payload: response} )
          }else{
            const response = {
              isOK: false,
              error: responseJson
            }
            console.log('ERROR UPDATE_CREDIT DATA: ',response);
            dispatch( { type: UPDATE_CREDIT_ERROR, payload: response} )
          }
          
        })
        .catch((error) => {
          const responseError = {
            isOK: false,
            error: error
          }
          console.log('ERROR UPDATE_CREDIT DATA: ',responseError);
          dispatch({ type: UPDATE_CREDIT_ERROR, payload: responseError })
        });
    }

  }

};