
import { DELETE_USER } from '../endPoints'
import {
  DELETE_USER_SUCCESS,
  DELETE_USER_ERROR,
  DELETE_USER_PURGE
} from '../types'


export const deleteUserAction = (data, token) => {
  
  const config={
    method: 'POST',
    body:JSON.stringify(data),
    headers:{
      'access-token': token
    }
  };
  
  console.log('RECUEST:',data, config, DELETE_USER);
  
  return ( dispatch ) => {

    if(data === 'PURGE'){
      dispatch( { type: DELETE_USER_PURGE, payload: 'PURGE'} )
    }else{
      fetch(DELETE_USER ,config)
        .then(json => json.json())
        .then((responseJson) => {
          if(responseJson){
            const response = {
              isOK:true,
              DeleteUser: responseJson
            }
            console.log('RESPONSE DELETE_USER DATA: ',response);
            dispatch( { type: DELETE_USER_SUCCESS, payload: response} )
          }else{
            const response = {
              isOK: false,
              error: responseJson
            }
            console.log('ERROR DELETE_USER DATA: ',response);
            dispatch( { type: DELETE_USER_ERROR, payload: response} )
          }
          
        })
        .catch((error) => {
          const responseError = {
            isOK: false,
            error: error
          }
          console.log('ERROR DELETE_USER DATA: ',responseError);
          dispatch({ type: DELETE_USER_ERROR, payload: responseError })
        });
    }

  }

};