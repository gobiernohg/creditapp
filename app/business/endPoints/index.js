import {Platform} from 'react-native'
export const URL_SERVER = Platform.OS === 'android' ? 'http://10.0.2.2:3000/': 'http://localhost:3000';
export const LOGIN = `${URL_SERVER}signin`;
export const GET_ALL_USERS = `${URL_SERVER}getAllUsers`;
export const ADD_USERS = `${URL_SERVER}addUser`;
export const DELETE_USER = `${URL_SERVER}deleteUser`;
export const ADD_CREDIT = `${URL_SERVER}addCredit`;
export const UPDATE_CREDIT = `${URL_SERVER}updateStatusCredit`;
export const GET_USER = `${URL_SERVER}getUser`;