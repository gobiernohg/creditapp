import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {width} from 'react-native-dimension';

//StackSplash
import SplashScreen from './screens/splash/SplashScreen';
import SignScreen from './screens/sign/SignScreen';

//StackHome
import HomeScreen from './screens/home/HomeScreen';
import NewUserScreen from './screens/user/NewUserScreen';
import DetailUserScreen from './screens/user/DetailUserScreen';
import AddCreditScreen from './screens/user/AddCreditScreen';

//MENU
import MenuScreen from './screens/menu/DrawerScreen';

const HomeNavigator = createStackNavigator({
  Home: {screen: HomeScreen, navigationOptions: {headerBackTitle: null}},
  NewUser: {screen: NewUserScreen, navigationOptions: {headerBackTitle: null}},
  DetailUser:{screen: DetailUserScreen, navigationOptions: {headerBackTitle: null}},
  AddCredit:{screen: AddCreditScreen, navigationOptions: {headerBackTitle: null}},
});


const HomeDrawer = createDrawerNavigator({
    Home: {screen: HomeNavigator, navigationOptions: {headerBackTitle: null}},
  },
  {
    contentComponent: MenuScreen,
    drawerWidth: width(80),
    overlayColor: 'rgba(0,0,0,0.8)',
  }
);

export const SignNavigator = createStackNavigator({
  Splash: {screen: SplashScreen, navigationOptions: {headerBackTitle: null}},
  Sign: {screen: SignScreen, navigationOptions: {headerBackTitle: null}},
});

const MainNavigator = createSwitchNavigator({
  Initial: {screen: SignNavigator, navigationOptions: {headerBackTitle: null}},
  Home: {screen: HomeDrawer, navigationOptions: {headerBackTitle: null}},
});

export default createAppContainer(MainNavigator);
