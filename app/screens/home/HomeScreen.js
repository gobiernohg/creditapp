import React, {Component} from 'react';
import {View, Text, StyleSheet, StatusBar, Image, FlatList, AppState} from 'react-native';
import color from '../../utils/common/ColorsCommonUtil';
import {width, height} from 'react-native-dimension';
import {resetSwitchNavigation, resetAndNavigateTo, goAndNavigateTo, goAndNavigateTowParams} from '../../NavigatorUtil';
import {getAllUsersAction} from '../../business/actions/GetAllUsersAction';
import {deleteUserAction} from '../../business/actions/DeleteUserAction';
import {AlertErrorCallBack} from '../../utils/common/FunctionCommon';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
MaterialIcons.loadFont()
AntDesign.loadFont()

import { connect } from "react-redux";
import LinearGradient from 'react-native-linear-gradient';
import { TouchableHighlight } from 'react-native-gesture-handler';

type HomeScreenProps={
  navigation:any,
}

type HomeScreenState={

}

class HomeScreen extends Component<HomeScreenProps,HomeScreenState> {
  static navigationOptions = {
    headerShown: false,
  };

  state={
    users:[],
  }

  componentDidMount(){
    console.log('TOKEN',this.props.loginData)
    AppState.addEventListener("change", ()=>{});
    this.willFocus = this.props.navigation.addListener("willFocus", () => {
      this._getAllUsers()
    });
   
  }

  _getAllUsers =()=>{
    let Token = this.props.loginData.Login.token
    this.props.getAllUsersAction({}, Token)
  }
  
  static getDerivedStateFromProps = (nextProps, prevState) => {
    const {getAllUsersData, getAllUsersError} = nextProps
    if( getAllUsersData?.AllUsers?.users === prevState.users){
      return null
    }else{
      console.log('HAVE USERS', getAllUsersData?.AllUsers?.users)
      if(getAllUsersData?.AllUsers?.users){
        return {
          users:getAllUsersData?.AllUsers?.users
        }
      }else{
        return null
      }
    }
  }

  _gotoAddUser=()=>{
    goAndNavigateTo(this.props.navigation,'NewUser',{})
  }

  _deleteUser =(user)=>{
    let Token = this.props.loginData.Login.token
    let idToDelete = {
      _id: user._id
    }
    this.props.deleteUserAction(idToDelete, Token)
    
    setTimeout(()=>{
      this._getAllUsers()
    }, 400)
  }
  
  render() {
    const {container, imageStyle, titleStyle} = styles;
    const colorsSplash = [color.green,color.greenSecond,color.blueOne,color.blueFinal]
    return (
      <LinearGradient colors={colorsSplash} style={container}>
       <StatusBar backgroundColor={color.greenDark} barStyle="light-content" />
       <View style={{width:width(90), flexDirection:'row', alignItems:'center'}}>
        <Text style={[titleStyle, {width:width(80)}]}>{'HOME'}</Text>
        <TouchableHighlight
          underlayColor={'transparent'}
          onPress={()=>{
            this._gotoAddUser()
          }}>
          <AntDesign name={'adduser'} size={40} color={color.grayDark}/>
        </TouchableHighlight>
       </View>
        
        {this._renderUsers()}
      </LinearGradient>
    );
  }

  _renderUsers=()=>{
    const {users} = this.state
    const {titleStyle} = styles;
    if(users.length>0){
      return(
        <View>
          <FlatList
            data={users}
            renderItem={({ item }) => (
              this._renderUser(item)
            )}
            keyExtractor={item => item._id}
            extraData={this.state}
            onEndReachedThreshold={0.5}
          />
        </View>
      )
    }else{
      return(
           <Text style={titleStyle}>{'NO USERS'}</Text>
      )
    }
  }

  _renderUser=(user)=>{
    return(
      <View style={{width:width(90), marginVertical:15, alignItems:'center', flexDirection:'row', borderBottomColor:color.grayLine, borderBottomWidth:1}}>
        <TouchableHighlight
        underlayColor={'transparent'}
        onPress={()=>{
          AlertErrorCallBack('Estas seguro de eliminar al usuario', ()=>{this._deleteUser(user)})
        }}>
          <MaterialIcons name={'delete-forever'} size={40} color={color.red}/>
        </TouchableHighlight>
      
        <Image
          style={{width:width(12), height:width(12), borderRadius:width(10)/2}}
          source={require('../../utils/img/user.png')}
        />
        <Text style={{width:width(50),marginVertical:20, color:color.grayDark, fontWeight:'100', fontSize:20, marginHorizontal:20}}>{user.name}</Text>
        <TouchableHighlight
        underlayColor={'transparent'}
          onPress={()=>{
            goAndNavigateTowParams(this.props.navigation, 'DetailUser', {dataUser: user})
          }}>
        <MaterialIcons name={'navigate-next'} size={40} color={color.grayDark}/>
        </TouchableHighlight>
      </View>
    )
  }

}

const styles = StyleSheet.create({
  container: {flex: 1, alignItems: 'center',backgroundColor: color.splashColor, paddingTop:height(5)},
  imageStyle:{width:width(20), height:width(20), tintColor:color.white},
  titleStyle:{marginVertical:40, color:color.grayDark, fontWeight:'100', fontSize:20, textAlign:'center'}
});

const mapStateToProps = (state) => {
  return {
    loginData: state.login.loginData,
    loginError: state.login.loginError,

    getAllUsersData: state.getAllUsers.getAllUsersData,
    getAllUsersError: state.getAllUsers.getAllUsersError,

    deleteUserData: state.deleteUser.deleteUserData,
    deleteUserError: state.deleteUser.deleteUserError
  }
};

export default connect(mapStateToProps, {getAllUsersAction, deleteUserAction})(HomeScreen);

