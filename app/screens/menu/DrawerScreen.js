import React, {Component} from 'react';
import {View, Text, StyleSheet, StatusBar, Image} from 'react-native';
import color from '../../utils/common/ColorsCommonUtil';
import {width, height} from 'react-native-dimension';
import {resetSwitchNavigation, resetAndNavigateTo} from '../../NavigatorUtil';

import { connect } from "react-redux";
import LinearGradient from 'react-native-linear-gradient';

type HomeScreenProps={
  navigation:any,
}

type HomeScreenState={

}

class HomeScreen extends Component<HomeScreenProps,HomeScreenState> {
  static navigationOptions = {
    headerShown: false,
  };
  
  render() {
    const {container, imageStyle, titleStyle} = styles;
    return (
      <View style={container}>
       <StatusBar backgroundColor={color.greenDark} barStyle="light-content" />
        <Text style={titleStyle}>{'MENU'}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {flex: 1, alignItems: 'center', justifyContent:'center',backgroundColor: color.white},
  imageStyle:{width:width(20), height:width(20), tintColor:color.white},
  titleStyle:{marginVertical:40, color:color.grayDark, fontWeight:'100', fontSize:20, textAlign:'center'}
});

const mapStateToProps = (state) => {
  return {
   
  }
};

export default connect(mapStateToProps, {})(HomeScreen);

