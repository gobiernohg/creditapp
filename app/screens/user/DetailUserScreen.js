import React, {Component} from 'react';
import {View, Text, StyleSheet, StatusBar, Image, TouchableHighlight, TextInput, AppState} from 'react-native';
import color from '../../utils/common/ColorsCommonUtil';
import {width, height} from 'react-native-dimension';
import {resetSwitchNavigation, resetAndNavigateTo, goAndNavigateTo, goBack, goAndNavigateTowParams} from '../../NavigatorUtil';
import {updateCreditAction} from '../../business/actions/UpdateCreditAction';
import {getUserAction} from '../../business/actions/GetUserAction';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
Ionicons.loadFont()
MaterialIcons.loadFont()
AntDesign.loadFont()
MaterialCommunityIcons.loadFont()
import {messageAlert} from '../../utils/common/FunctionCommon'
import { connect } from "react-redux";
import LinearGradient from 'react-native-linear-gradient';
import ButtonComponent from '../../components/ButtonComponent';
import { ScrollView } from 'react-native-gesture-handler';

type DetailUserScreenProps={
  navigation:any,
}

type DetailUserScreenState={

}

class DetailUserScreen extends Component<DetailUserScreenProps,DetailUserScreenState> {
  static navigationOptions = {
    headerShown: false,
  };

  state={
    user:{},
    updateCredit:{}
  }

  componentDidMount(){
   const {dataUser} = this.props.navigation.state.params;
   console.log('DETAIL USER', dataUser)
   this.setState({user:dataUser})
   AppState.addEventListener("change", ()=>{});
   this.willFocus = this.props.navigation.addListener("willFocus", () => {
    this._getDataUser()
   });
   
  }

  static getDerivedStateFromProps = (nextProps, prevState) => {
    const { getUserData, getUserError} = nextProps
      if(getUserData?.GetUser === prevState.GetUser){
        return null
      }else {
        let user={}
        if(getUserData?.GetUser?.user && getUserData?.GetUser?.error === false  ){
          user=getUserData?.GetUser?.user
        }  
        return{
          user:user
        }
      }
  }

  _getDataUser =()=>{
    let Token = this.props.loginData.Login.token
    const {dataUser} = this.props.navigation.state.params;
    let update ={
      _id: dataUser._id,
    }
    this.props.getUserAction(update, Token)
  }

  _changeStatus = (credit, status)=>{
    let Token = this.props.loginData.Login.token
    const {user} = this.state;
    let update ={
      _id: user._id,
      idCredit: credit.idCredit,
      status: status
    }
    this.props.updateCreditAction(update, Token)
    setTimeout(()=>{
      this._getDataUser()
      this.props.updateCreditAction('PURGE')
    },400)
  }
  
  render() {
    const {container, imageStyle, titleStyle} = styles;
    const colorsSplash = [color.green,color.greenSecond,color.blueOne,color.blueFinal]
    const{name, tel} = this.state
    return (
      <LinearGradient colors={colorsSplash} style={container}>
       <StatusBar backgroundColor={color.greenDark} barStyle="light-content" />
       <View style={{width:width(90), flexDirection:'row', alignItems:'center'}}>
        <Text style={[titleStyle, {width:width(80)}]}>{'INFORMACIONDE USUARIO'}</Text>
       </View>
       {this._renderUserInfo()}
       <ScrollView>
       <View style={{width:width(100), alignItems:'center'}}>
        {this._renderCredits()}
        {this._renderButtons()}
       </View>
        
       </ScrollView>
       
        {this._renderBack()}
      </LinearGradient>
    );
  }

  _renderBack=()=>{
    return(
      <View style={{ flexDirection:'row', width:width(90), alignItems:'center', position:'absolute', top: height(5) }}>
         <TouchableHighlight
            style={{marginTop:10}}
            underlayColor={'transparent'}
            onPress={()=>{
              //this.props.AddUsersAction('PURGE')
              goBack(this.props.navigation)
            }}>
            <Ionicons name={Platform.OS==='android'?'ios-arrow-round-back':'ios-arrow-back'} size={40} color={color.grayDark}/>
          </TouchableHighlight>
      </View>
    )
  }

  _renderUserInfo =()=>{
    const {user} = this.state;
    return(
        <View style={{flexDirection:'row', alignItems:'center', width:width(90), borderBottomWidth:1, borderBottomColor:color.grayLine}}>
          <Image
            style={{width:width(20), height:width(20), borderRadius:width(20)/2}}
            source={require('../../utils/img/user.png')}
          />
          <View>
          <Text style={{width:width(50),marginVertical:10, color:color.grayDark, fontWeight:'100', fontSize:15, marginHorizontal:20}}>{user.name}</Text>
          <Text style={{width:width(50),marginVertical:10, color:color.grayDark, fontWeight:'100', fontSize:13, marginHorizontal:20}}>{user.tel}</Text>
          </View>
        
        </View>
    )
  }

  _renderCredits = ()=>{
    const {user} = this.state;
    if(user?.credits && user?.credits.length>0){
      let credits = user?.credits
      let listCredits = credits.map((credit, index)=>{
          let status = 'EN PROCESOS';
          let showActions = false;
          let colors= color.yellow
          if(credit.status === 'cancelado'){
            status = 'CANCELADO';
            showActions= true;
            colors= color.red
          }else if(credit.status === 'dispensado'){
            status = 'DISPENSADO';
            showActions= true
            colors= color.colorSeparatorTwo
          }
          return(
            <View style={{width:width(90), marginVertical:10, flexDirection:'row', borderBottomColor:color.grayLine, borderBottomWidth:1, flexDirection:'row'}}>
               <Text style={{width:width(55),marginVertical:10, color:color.grayDark, fontWeight:'100', fontSize:15, marginHorizontal:20}}>{credit.monto}</Text>
               {showActions?
                <View style={{backgroundColor:colors, padding:5, height:30, borderRadius:5}}>
                  <Text style={{color:color.white, fontWeight:'400', fontSize:12}}>{status}</Text>
                </View>
               : 
                <View style={{flexDirection:'row', height:30}}>
                  <TouchableHighlight
                    onPress={()=>{
                      this._changeStatus(credit, 'dispensado')
                    }}
                    style={{backgroundColor:color.colorSeparatorTwo, padding:5, marginHorizontal:5, alignItems:'center', borderRadius:5}}>
                    <MaterialIcons name={'check'} size={20} color={color.white}/>
                  </TouchableHighlight>
                  <TouchableHighlight
                    onPress={()=>{
                      this._changeStatus(credit, 'cancelado')
                      }}
                    style={{backgroundColor:color.red, padding:5, marginHorizontal:20, alignItems:'center', borderRadius:5}}>
                    <MaterialCommunityIcons name={'cancel'} size={20} color={color.white}/>
                  </TouchableHighlight>
                </View>
               }
            </View>
          )
      })
      return(
        <View>
          <Text style={{width:width(90),marginVertical:10, color:color.grayDark, fontWeight:'100', fontSize:15, marginHorizontal:5}}>{'CREDITOS'}</Text>
          {listCredits}
        </View>
      )
    }
  }

  _renderButtons =()=>{
    return(
      <View style={{width:width(100), alignItems:'center',marginTop:height(5)}}>
        <ButtonComponent
          action={()=>{
            goAndNavigateTowParams(this.props.navigation, 'AddCredit', {dataUser: this.state.user})
          }}
          title={'AÑADIR CREDITO'}
          width={width(80)}
          color={color.colorBlueOne}
          colorText={color.white}
        />
      </View>
    )
  }

 

}

const styles = StyleSheet.create({
  container: {flex: 1, alignItems: 'center',backgroundColor: color.splashColor, paddingTop:height(5)},
  imageStyle:{width:width(20), height:width(20), tintColor:color.white},
  titleStyle:{marginVertical:20, color:color.grayDark, fontWeight:'100', fontSize:20, textAlign:'center'}
});

const mapStateToProps = (state) => {
  return {
    loginData: state.login.loginData,
    loginError: state.login.loginError,

    updateCreditData: state.updateCredit.updateCreditData,
    updateCreditError: state.updateCredit.updateCreditError,

    getUserData: state.getUser.getUserData,
    getUserError: state.getUser.getUserError,
    

  }
};

export default connect(mapStateToProps, {updateCreditAction, getUserAction})(DetailUserScreen);

