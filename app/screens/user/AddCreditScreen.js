import React, {Component} from 'react';
import {View, Text, StyleSheet, StatusBar, Image, TouchableHighlight, TextInput} from 'react-native';
import color from '../../utils/common/ColorsCommonUtil';
import {width, height} from 'react-native-dimension';
import {resetSwitchNavigation, resetAndNavigateTo, goAndNavigateTo, goBack} from '../../NavigatorUtil';
import {AddCreditAction} from '../../business/actions/AddCreditAction';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
Ionicons.loadFont()
MaterialIcons.loadFont()
AntDesign.loadFont()
import {messageAlert, numberToMoney} from '../../utils/common/FunctionCommon'
import { connect } from "react-redux";
import LinearGradient from 'react-native-linear-gradient';
import ButtonComponent from '../../components/ButtonComponent';

type AddCreditScreenProps={
  navigation:any,
}

type AddCreditScreenState={

}

class AddCreditScreen extends Component<AddCreditScreenProps,AddCreditScreenState> {
  static navigationOptions = {
    headerShown: false,
  };

  state={
    credit:'',
    user:{}
  }

  componentDidMount(){
    console.log('TOKEN',this.props.loginData)
    const {dataUser} = this.props.navigation.state.params;
    console.log('DETAIL USER', dataUser)
    this.setState({user:dataUser})
  }

  _addCredit =()=>{
    const{credit, user} = this.state
    if(credit.length>0){
        let Token = this.props.loginData.Login.token
        let newUser ={
          _id: user._id,
          monto:numberToMoney(credit),
          status:'en_proceso'
        }
        this.props.AddCreditAction(newUser, Token)
    }else{
      messageAlert('Ingresa el monto que quieres de credito')
    }
  }
  
  static getDerivedStateFromProps = (nextProps, prevState) => {
    const {addCreditData, addCreditError} = nextProps
    if( addCreditData?.AddCredit?._id === prevState.users?._id){
      return null
    }else{
      console.log('ADD CREDIT', addCreditData?.AddCredit)
      if(addCreditData?.AddCredit?._id){
        
        messageAlert('Se envio solicitud de credito')
        goBack(nextProps.navigation)
        nextProps.AddCreditAction('PURGE')
        return {
          addUser:addCreditData?.AddCredit
        }
      }else{
        return null
      }
    }
  }
  
  render() {
    const {container, imageStyle, titleStyle} = styles;
    const colorsSplash = [color.green,color.greenSecond,color.blueOne,color.blueFinal]
    const{credit} = this.state
    return (
      <LinearGradient colors={colorsSplash} style={container}>
       <StatusBar backgroundColor={color.greenDark} barStyle="light-content" />
       <View style={{width:width(90), flexDirection:'row', alignItems:'center'}}>
        <Text style={[titleStyle, {width:width(80)}]}>{'ADD USER'}</Text>
       
       </View>
       {this._renderInput('numeric',credit, 'credit','Credito',true, false )}
        {this._renderButtons()}
        {this._renderBack()}
      </LinearGradient>
    );
  }

  _renderBack=()=>{
    return(
      <View style={{ flexDirection:'row', width:width(90), alignItems:'center', position:'absolute', top: height(5) }}>
         <TouchableHighlight
            style={{marginTop:10}}
            underlayColor={'transparent'}
            onPress={()=>{
              this.props.AddCreditAction('PURGE')
              goBack(this.props.navigation)
            }}>
            <Ionicons name={Platform.OS==='android'?'ios-arrow-round-back':'ios-arrow-back'} size={40} color={color.grayDark}/>
          </TouchableHighlight>
      </View>
    )
  }

  _renderInput=(keyboardType, value ,state,placeholder, autoFocus, secure)=>{
    return(
      <View style={[{ padding:5, marginVertical:10, borderRadius:10, backgroundColor:color.white, paddingHorizontal:10}]}>
        <TextInput
          keyboardAppearance={'dark'}
          keyboardType={keyboardType}
          autoFocus={autoFocus}
          placeholder={placeholder}
          placeholderTextColor={color.backColor}
          value={value}
          onChangeText={(text)=>{
            this.setState({[state]: text})
          }}
          secureTextEntry={secure}
          style={[{color:color.grayStrong,fontSize:15, width:width(80), height:40}]}
        />
      </View>
    )
  }

  _renderButtons =()=>{
    return(
      <View style={{width:width(100), alignItems:'center',marginTop:height(5)}}>
        <ButtonComponent
          action={()=>{
            this._addCredit()
          }}
          title={'AÑADIR CREDITO'}
          width={width(80)}
          color={color.colorBlueTwo}
          colorText={color.white}
        />
      </View>
    )
  }

 

}

const styles = StyleSheet.create({
  container: {flex: 1, alignItems: 'center',backgroundColor: color.splashColor, paddingTop:height(5)},
  imageStyle:{width:width(20), height:width(20), tintColor:color.white},
  titleStyle:{marginVertical:40, color:color.grayDark, fontWeight:'100', fontSize:20, textAlign:'center'}
});

const mapStateToProps = (state) => {
  return {
    loginData: state.login.loginData,
    loginError: state.login.loginError,

    addCreditData: state.addCredit.addCreditData,
    addCreditError: state.addCredit.addCreditError,
  }
};

export default connect(mapStateToProps, {AddCreditAction})(AddCreditScreen);

