import React, {Component} from 'react';
import {View, Text, StyleSheet, StatusBar, Image, TouchableHighlight, TextInput} from 'react-native';
import color from '../../utils/common/ColorsCommonUtil';
import {width, height} from 'react-native-dimension';
import {resetSwitchNavigation, resetAndNavigateTo, goAndNavigateTo, goBack} from '../../NavigatorUtil';
import {AddUsersAction} from '../../business/actions/AddUsersAction';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
Ionicons.loadFont()
MaterialIcons.loadFont()
AntDesign.loadFont()
import {messageAlert} from '../../utils/common/FunctionCommon'
import { connect } from "react-redux";
import LinearGradient from 'react-native-linear-gradient';
import ButtonComponent from '../../components/ButtonComponent';

type NewUserScreenProps={
  navigation:any,
}

type NewUserScreenState={

}

class NewUserScreen extends Component<NewUserScreenProps,NewUserScreenState> {
  static navigationOptions = {
    headerShown: false,
  };

  state={
    name:'',
    tel:'',
    addUser:{}
  }

  componentDidMount(){
    console.log('TOKEN',this.props.loginData)
   
  }

  _addUser =()=>{
    const{name, tel} = this.state
    if(name.length>0){
      if(tel.length>0){
        let Token = this.props.loginData.Login.token
        let newUser ={
          name:name,
          tel:tel
        }
        this.props.AddUsersAction(newUser, Token)
      }else{
        messageAlert('Ingresa su numero telefonico')
      }
    }else{
      messageAlert('Ingresa el nombre')
    }
  }
  
  static getDerivedStateFromProps = (nextProps, prevState) => {
    const {addUsersData, addUsersError} = nextProps
    if( addUsersData?.AddUsers?._id === prevState.users?._id){
      return null
    }else{
      console.log('ADD USER', addUsersData?.AddUsers)
      if(addUsersData?.AddUsers?._id){
        
        messageAlert('El usuario se añadio exitosamente')
        goBack(nextProps.navigation)
        nextProps.AddUsersAction('PURGE')
        return {
          addUser:addUsersData?.AddUsers
        }
      }else{
        return null
      }
    }
  }
  
  render() {
    const {container, imageStyle, titleStyle} = styles;
    const colorsSplash = [color.green,color.greenSecond,color.blueOne,color.blueFinal]
    const{name, tel} = this.state
    return (
      <LinearGradient colors={colorsSplash} style={container}>
       <StatusBar backgroundColor={color.greenDark} barStyle="light-content" />
       <View style={{width:width(90), flexDirection:'row', alignItems:'center'}}>
        <Text style={[titleStyle, {width:width(80)}]}>{'ADD USER'}</Text>
       
       </View>
       {this._renderInput('default',name, 'name','Nombre',true, false )}
        {this._renderInput('number-pad',tel, 'tel','Telefono',false, false )}
        {this._renderButtons()}
        {this._renderBack()}
      </LinearGradient>
    );
  }

  _renderBack=()=>{
    return(
      <View style={{ flexDirection:'row', width:width(90), alignItems:'center', position:'absolute', top: height(5) }}>
         <TouchableHighlight
            style={{marginTop:10}}
            underlayColor={'transparent'}
            onPress={()=>{
              this.props.AddUsersAction('PURGE')
              goBack(this.props.navigation)
            }}>
            <Ionicons name={Platform.OS==='android'?'ios-arrow-round-back':'ios-arrow-back'} size={40} color={color.grayDark}/>
          </TouchableHighlight>
      </View>
    )
  }

  _renderInput=(keyboardType, value ,state,placeholder, autoFocus, secure)=>{
    return(
      <View style={[{ padding:5, marginVertical:10, borderRadius:10, backgroundColor:color.white, paddingHorizontal:10}]}>
        <TextInput
          keyboardAppearance={'dark'}
          keyboardType={keyboardType}
          autoFocus={autoFocus}
          placeholder={placeholder}
          placeholderTextColor={color.backColor}
          value={value}
          onChangeText={(text)=>{
            this.setState({[state]: text})
          }}
          secureTextEntry={secure}
          style={[{color:color.grayStrong,fontSize:15, width:width(80), height:40}]}
        />
      </View>
    )
  }

  _renderButtons =()=>{
    return(
      <View style={{width:width(100), alignItems:'center',marginTop:height(5)}}>
        <ButtonComponent
          action={()=>{
            this._addUser()
          }}
          title={'AÑADIR USUARIO'}
          width={width(80)}
          color={color.colorBlueTwo}
          colorText={color.white}
        />
      </View>
    )
  }

 

}

const styles = StyleSheet.create({
  container: {flex: 1, alignItems: 'center',backgroundColor: color.splashColor, paddingTop:height(5)},
  imageStyle:{width:width(20), height:width(20), tintColor:color.white},
  titleStyle:{marginVertical:40, color:color.grayDark, fontWeight:'100', fontSize:20, textAlign:'center'}
});

const mapStateToProps = (state) => {
  return {
    loginData: state.login.loginData,
    loginError: state.login.loginError,

    addUsersData: state.addUsers.addUsersData,
    addUsersError: state.addUsers.addUsersError
  }
};

export default connect(mapStateToProps, {AddUsersAction})(NewUserScreen);

