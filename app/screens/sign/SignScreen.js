import React, {Component} from 'react';
import {View, Text, StyleSheet, StatusBar, Image, TextInput} from 'react-native';
import color from '../../utils/common/ColorsCommonUtil';
import {width, height} from 'react-native-dimension';
import {resetSwitchNavigation, resetAndNavigateTo} from '../../NavigatorUtil';
import {messageAlert} from '../../utils/common/FunctionCommon'

import { connect } from "react-redux";
import {loginAction} from '../../business/actions/LoginAction';
import LinearGradient from 'react-native-linear-gradient';
import ButtonComponent from '../../components/ButtonComponent';

type SignScreenProps={
  navigation:any,
}

type SignScreenState={

}

class SignScreen extends Component<SignScreenProps,SignScreenState> {
  static navigationOptions = {
    headerShown: false,
  };

  state={
    user:'',
    password:'',
    token:''
  }

  componentDidMount(){

  }
  
  static getDerivedStateFromProps = (nextProps, prevState) => {
    const {loginData, loginError} = nextProps
    if( loginData?.Login?.token=== prevState.token){
      return null
    }else{
      console.log('HAVE LOGIN', loginData?.Login)
      if(loginData?.Login?.mensaje && loginData?.Login?.error){
        messageAlert(loginData?.Login?.mensaje)
        nextProps.loginAction('PURGE')
      }
      if(loginData?.Login?.token ){
          resetSwitchNavigation(nextProps.navigation,'Home',{})
        return {
          token:loginData?.Login?.token
        }
      }else{
        return null
      }
    }
  }

  _signUp=()=>{
    const {user,password} = this.state;
    if(user.length>0){
      if(password.length>0){
        let dataLogin ={
          user:user.toLocaleLowerCase(),
          password:password.toLocaleLowerCase()
        }
        this.props.loginAction(dataLogin)
      }else{
        messageAlert('Ingresa un password')
      }
    }else{
      messageAlert('Ingresa un usuario')
    }
  }
  
  render() {
    const {container, imageStyle, titleStyle} = styles;
    const colorsSplash = [color.green,color.greenSecond,color.blueOne,color.blueFinal]
    const {user,password} = this.state
    return (
      <LinearGradient colors={colorsSplash} style={container}>
       <StatusBar backgroundColor={color.greenDark} barStyle="light-content" />
        <Text style={titleStyle}>{'SIGN'}</Text>
        {this._renderInput('default',user, 'user','Usuario',true, false )}
        {this._renderInput('default',password, 'password','Password',false, false )}
        {this._renderButtons()}
      </LinearGradient>
    );
  }

  _renderInput=(keyboardType, value ,state,placeholder, autoFocus, secure)=>{
    return(
      <View style={[{ padding:5, marginVertical:10, borderRadius:10, backgroundColor:color.white, paddingHorizontal:10}]}>
        <TextInput
          keyboardAppearance={'dark'}
          keyboardType={keyboardType}
          autoFocus={autoFocus}
          placeholder={placeholder}
          placeholderTextColor={color.backColor}
          value={value}
          onChangeText={(text)=>{
            this.setState({[state]: text})
          }}
          secureTextEntry={secure}
          style={[{color:color.grayStrong,fontSize:15, width:width(80), height:40}]}
        />
      </View>
    )
  }

  _renderButtons =()=>{
    return(
      <View style={{width:width(100), alignItems:'center',marginTop:height(5)}}>
        <ButtonComponent
          action={()=>{
            this._signUp()
          }}
          title={'LOGIN'}
          width={width(80)}
          color={color.colorBlueTwo}
          colorText={color.white}
        />
      </View>
    )
  }
}



const styles = StyleSheet.create({
  container: {flex: 1, alignItems: 'center',backgroundColor: color.splashColor, paddingTop:height(20)},
  imageStyle:{width:width(20), height:width(20), tintColor:color.white},
  titleStyle:{marginVertical:40, color:color.grayDark, fontWeight:'100', fontSize:20, textAlign:'center'}
});

const mapStateToProps = (state) => {
  return {
    loginData: state.login.loginData,
    loginError: state.login.loginError
  }
};

export default connect(mapStateToProps, {loginAction})(SignScreen);

