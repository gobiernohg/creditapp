import React, {Component} from 'react';
import {View, Text, StyleSheet, StatusBar, Image} from 'react-native';
import color from '../../utils/common/ColorsCommonUtil';
import {width, height} from 'react-native-dimension';
import {resetSwitchNavigation, resetAndNavigateTo, goAndNavigateTowParams} from '../../NavigatorUtil';
import {haveServerAction} from '../../business/actions/HaveServerAction'

import { connect } from "react-redux";
import LinearGradient from 'react-native-linear-gradient';

type SplashScreenProps={
  navigation:any,
}

type SplashScreenState={

}

class SplashScreen extends Component<SplashScreenProps,SplashScreenState> {
  static navigationOptions = {
    headerShown: false,
  };

  state={
    haveServer:false
  }

  componentDidMount(){
    this.props.haveServerAction({})
  }
  
  static getDerivedStateFromProps = (nextProps, prevState) => {
    const {haveServerData, haveServerError} = nextProps
    if( haveServerData?.HaveServer === prevState.haveServer){
      return null
    }else{
      console.log('HAVE SERVER', haveServerData?.HaveServer?.online)
      if(haveServerData?.HaveServer?.online){
        setTimeout(()=>{

          goAndNavigateTowParams(nextProps.navigation,'Sign',{})

        },3000)
        return {
          haveServer:haveServerData?.HaveServer?.online
        }
      }else{
        return null
      }
    }
  }
  
  render() {
    const {container, imageStyle, titleStyle} = styles;
    const colorsSplash = [color.green,color.greenSecond,color.blueOne,color.blueFinal]
    return (
      <LinearGradient colors={colorsSplash} style={container}>
       <StatusBar backgroundColor={color.greenDark} barStyle="light-content" />
        <Text style={titleStyle}>{'Credit \n by Alejandro Tenorio'}</Text>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  container: {flex: 1, alignItems: 'center', justifyContent:'center',backgroundColor: color.splashColor},
  imageStyle:{width:width(20), height:width(20), tintColor:color.white},
  titleStyle:{marginVertical:40, color:color.grayDark, fontWeight:'100', fontSize:20, textAlign:'center'}
});

const mapStateToProps = (state) => {
  return {
    haveServerData: state.haveServer.haveServerData,
    haveServerError: state.haveServer.haveServerError
  }
};

export default connect(mapStateToProps, {haveServerAction})(SplashScreen);

